<?php if ( ! defined ('BASEPATH')) exit ('No direct script access allowed');
/**
 * Defines a social bookmarks plugin
 *
 * @author Eric Slenk <slenkeri@anr.msu.edu>
 * @version 1.0.3
 */
$plugin_info = array (
	'pi_name'		=> 'Social Bookmarks',
	'pi_version'		=> '1.0.3',
	'pi_author' 		=> 'Eric Slenk',
	'pi_description' 	=> 'Provides Social Links'
);


/**
 * Outputs a social bookmarks bar
 */
class Social_bookmarks
{
	/**
	 * Data output by the plugin.
	 *
	 * @var string
	 */
	public $return_data = '';

	/**
	 * Layouts which are recognized by the plugin.
	 *
	 * @var array
	 */
	private $_valid_layouts = array('wide', 'tall', 'small');

	/**
	 * Layout to be used in the event that one is not specified.
	 * 
	 * @var string Default is 'wide'.
	 */
	private $_default_layout = 'wide';

	
	/**
	 * Constructor.
	 *
	 * Renders social bookmarks.
	 */
	public function __construct ()
	{
		// Get parameters
		$layout 			= in_array( get_instance()->TMPL->fetch_param('layout', $this->_default_layout), $this->_valid_layouts ) ?
								get_instance()->TMPL->fetch_param('layout', $this->_default_layout) :
								$this->_default_layout;
		$included_buttons 	= explode('|', get_instance()->TMPL->fetch_param('buttons', 'facebook|facebook-share|googleplus|linkedin|stumbleupon|reddit|twitter|pinterest|tumblr') );
		$data_url 			= 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['PATH_INFO'];

		// Render templates for all included buttons
		$this->return_data = $this->_render_container(
			$layout,
			$this->_render_buttons(
				$layout,
				$included_buttons,
				$data_url
			),
			$this->_render_styles()
		);
	}

	/**
	 * Renders the given buttons' templates as HTML.
	 *
	 * @param string $layout           Name of the layout for the social bookmarks bar.
	 * @param array  $included_buttons Array of names of button templates to include.
	 * @param string $data_url         URL of the current page which some buttons use for sharing.
	 *
	 * @return string HTML for all rendered buttons.
	 */
	private function _render_buttons ($layout, $included_buttons, $data_url)
	{
		$button_html = '';

		// Render every button in the list
		foreach ( $included_buttons as $button )
		{
			if ( file_exists( dirname(__FILE__)."/views/buttons/{$button}.html" ) )
			{
					
				// Parse button's template file and concatenate output
				$button_html .= get_instance()->TMPL->parse_variables(
					file_get_contents( dirname(__FILE__)."/views/buttons/{$button}.html" ), // Button template file
					array(array( // Data to pass into the template. EE requires this be a multidimentional array.
						'layout' => $layout,
						'data_url' => $data_url,
						'data_url_encoded' => urlencode($data_url)
					))
				);
			}
		}

		return $button_html;
	}

	/**
	 * Renders the stylesheet as HTML.
	 * 
	 * @return string HTML for the stylesheet.
	 */
	private function _render_styles ()
	{
		$styles_css = '';

		if ( file_exists( dirname(__FILE__).'/scss/styles.css') )
		{
			$styles_css = get_instance()->TMPL->parse_variables(
				file_get_contents( dirname(__FILE__).'/scss/styles.css' ),
				array(array())
			);
		}

		return $styles_css;
	}

	/**
	 * Renders the container as HTML.
	 * 
	 * @param string $layout       Name of the layout for the social bookmarks bar.
	 * @param string $buttons_html HTML for all rendered buttons.
	 * @param string $styles_css   HTML for the stylesheet.
	 *
	 * @return string HTML for the social bookmarks container and its contents.
	 */
	private function _render_container ($layout, $buttons_html, $styles_css)
	{
		$container_html = '';

		// Render the container file
		if ( file_exists( dirname(__FILE__).'/views/container.html') )
		{
			$container_html = get_instance()->TMPL->parse_variables(
				file_get_contents( dirname(__FILE__).'/views/container.html' ),
				array(array(
					'layout' => $layout,
					'buttons' => $buttons_html,
					'styles' => $styles_css
				))
			);
		}

		return $container_html;
	}
}
